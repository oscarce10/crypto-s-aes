# Correr script


# -o encrypt -k KEY -m MESSAGE
# ex
# -o encrypt -k Jõ -m ×(


# -o decrypt -k KEY -m BASE64_CIPHERTEXT
# -o decrypt -k Jõ -m OTQ1Mg==




import re
import base64
import argparse

# %%
sBox = [9, 4, 10, 11, 13, 1, 8, 5, 6, 2, 0, 3, 12, 14, 15, 7]
invSBox = [10, 5, 9, 11, 1, 7, 8, 15, 6, 0, 2, 3, 12, 4, 13, 14]


# %%
def rot_nib(w):
    return (lambda x: x[1] + " " + x[0])(w.split(" "))


# %%
def fill_zeros(w, n=4):
    return w.zfill(n)


# %%
def sub_nib(w):
    """
    Substitutes nibbles in a word using sBox
    :param w:  word to substitute nibbles ex: 0101 0101
    :return:  substituted word ex: 1100 0011
    """
    return (lambda x: (
            (lambda y: fill_zeros(y))(bin(sBox[int(x[0], 2)])[2:]) + " " +
            (lambda y: fill_zeros(y))(bin(sBox[int(x[1], 2)])[2:])
    ))(w.split(" "))


# %%
def sub_inv_niv(w):
    return (lambda x: (
            (lambda y: fill_zeros(y))(bin(invSBox[int(x[0], 2)])[2:]) + " " +
            (lambda y: fill_zeros(y))(bin(invSBox[int(x[1], 2)])[2:])
    ))(w.split(" "))


# %%
def perform_xor(w1, w2):
    if len(w1) == 4 and len(w2) == 4:
        return fill_zeros(
            bin(int(w1, 2) ^ int(w2, 2))[2:]
        )
    else:
        x_0, x_1 = w1.split(" "), w2.split(" ")
        return fill_zeros(bin(int(x_0[0], 2) ^ int(x_1[0], 2))[2:]) + " " + fill_zeros(
            bin(int(x_0[1], 2) ^ int(x_1[1], 2))[2:])


# %%
def gf_multiplication_by_4(x):
    table = [0x0, 0x4, 0x8, 0xC, 0x3, 0x7, 0xB, 0xF, 0x6, 0x2, 0xE, 0xA, 0x5, 0x1, 0xD, 0x9]
    return fill_zeros(bin(table[int(x, 2)])[2:])


# %%
def gf_multiplication_by_9(x):
    table = [0x0, 0x9, 0x1, 0x8, 0x2, 0xB, 0x3, 0xA, 0x4, 0xD, 0x5, 0xC, 0x6, 0xF, 0x7, 0xE]
    return fill_zeros(bin(table[int(x, 2)])[2:])


# %%
def gf_multiplication_by_2(x):
    table = [0x0, 0x2, 0x4, 0x6, 0x8, 0xA, 0xC, 0xE, 0x3, 0x1, 0x7, 0x5, 0xB, 0x9, 0xF, 0xD]
    return fill_zeros(bin(table[int(x, 2)])[2:])


# %%

def inputs(operation="encrypt", key=None, plain_text=None):
    if operation == "encrypt":
        if key is None:
            key = input("Enter 2 character key: ")
        if len(key) != 2:
            raise ValueError("Key must consist of 2 characters")
        if plain_text is None:
            plain_text = input("Enter 2 character text: ")
        if len(plain_text) != 2:
            raise ValueError("Plain text to encrypt must consist of 2 characters")
        return (lambda x: fill_zeros(bin(ord(x[0]))[2:], 8) +
                          fill_zeros(bin(ord(x[1]))[2:], 8)
                )(key), (lambda x:
                         fill_zeros(bin(ord(x[0]))[2:], 8)
                         +
                         fill_zeros(bin(ord(x[1]))[2:], 8)
                         )(plain_text)
    else:
        if key is None:
            key = input("Enter 2 character key: ")
        if len(key) != 2:
            raise ValueError("Key must consist of 2 characters")
        if plain_text is None:
            plain_text = input("Enter encrypted message: ")

        try:
            bin_cipher = bin(int(base64.b64decode(plain_text).decode()))[2:].zfill(16)
        except Exception:
            raise ValueError("Invalid ciphertext")
        return (lambda x: fill_zeros(bin(ord(x[0]))[2:], 8) +
                          fill_zeros(bin(ord(x[1]))[2:], 8)
                )(key), bin_cipher


# %%
def keys_generation(k: str):
    w_0, w_1 = (lambda x: (x[0] + " " + x[1], x[2] + " " + x[3]))(re.findall('\\d{4}', k))
    w_2 = perform_xor(
        perform_xor(w_0, "1000 0000"),
        sub_nib(rot_nib(w_1))
    )
    w_3 = perform_xor(w_2, w_1)
    w_4 = perform_xor(
        perform_xor(w_2, "0011 0000"),
        sub_nib(rot_nib(w_3))
    )
    w_5 = perform_xor(w_4, w_3)
    k_0 = w_0 + " " + w_1
    k_1 = w_2 + " " + w_3
    k_2 = w_4 + " " + w_5
    return k_0, k_1, k_2


def encryption(plain_text_bin, k_0, k_1, k_2):
    # plaint text XOR Key_1
    pt_xor_key = []
    for i in range(4):
        pt_xor_key.append(perform_xor(plain_text_bin.split()[i], k_0.split()[i]))
    # %%
    # Nibble substitution
    nib_sub = []
    nib_sub += sub_nib(" ".join([pt_xor_key[0], pt_xor_key[1]])).split()
    nib_sub += sub_nib(" ".join([pt_xor_key[2], pt_xor_key[3]])).split()
    # %%
    # Shift rows
    S = nib_sub
    S[1], S[3] = S[3], S[1]
    # %%
    # Mix columns
    s_00 = perform_xor(S[0], gf_multiplication_by_4(S[1]))
    s_10 = perform_xor(gf_multiplication_by_4(S[0]), S[1])
    s_01 = perform_xor(S[2], gf_multiplication_by_4(S[3]))
    s_11 = perform_xor(gf_multiplication_by_4(S[2]), S[3])
    s_prime = [s_00, s_10, s_01, s_11]
    # %%
    # Add round 1 key
    add_round_key_1 = []
    for i in range(4):
        add_round_key_1.append(perform_xor(s_prime[i], k_1.split()[i]))
    # %%
    # Round 2 (Final Round)
    # Nibble substitution
    nib_sub = []
    nib_sub += sub_nib(" ".join([add_round_key_1[0], add_round_key_1[1]])).split()
    nib_sub += sub_nib(" ".join([add_round_key_1[2], add_round_key_1[3]])).split()
    # %%
    # Shift rows
    S = nib_sub
    S[1], S[3] = S[3], S[1]
    # %%
    # Add Round 2 Key_2
    # XOR with key_2
    add_round_key_2 = []
    for i in range(4):
        add_round_key_2.append(perform_xor(S[i], k_2.split()[i]))
    cipher_bin = add_round_key_2
    print("Cipher binary:  ", " ".join(cipher_bin))
    print("Cipher integer: ", int(cipher_bin[0] + cipher_bin[1], 2), int(cipher_bin[2] + cipher_bin[3], 2))
    print("Cipher text: ", chr(int(cipher_bin[0] + cipher_bin[1], 2)), chr(int(cipher_bin[2] + cipher_bin[3], 2)))
    enc_message = str(int("".join(cipher_bin), 2))
    return base64.b64encode(enc_message.encode()).decode()


# %%
def decryption(message_bin, k_0, k_1, k_2):
    # %%
    # Perform the XOR between k_2 and cipher binary
    add_round_k_2 = []
    for i in range(4):
        add_round_k_2.append(perform_xor(message_bin.split()[i], k_2.split()[i]))
    # %%
    # Shift rows
    add_round_k_2[1], add_round_k_2[3] = add_round_k_2[3], add_round_k_2[1]
    inv_shift_row = add_round_k_2
    # %%
    # Nibble substitution
    nib_sub = []
    nib_sub += sub_inv_niv(" ".join([inv_shift_row[0], inv_shift_row[1]])).split()
    nib_sub += sub_inv_niv(" ".join([inv_shift_row[2], inv_shift_row[3]])).split()
    inv_nib_sub = nib_sub

    # %%
    # Add Round 1 Key_1
    add_round_k_1 = []
    for i in range(4):
        add_round_k_1.append(perform_xor(inv_nib_sub[i], k_1.split()[i]))
    S = add_round_k_1
    # %%
    # Inverse Mix columns
    s_00 = perform_xor(gf_multiplication_by_9(S[0]), gf_multiplication_by_2(S[1]))
    s_10 = perform_xor(gf_multiplication_by_2(S[0]), gf_multiplication_by_9(S[1]))
    s_01 = perform_xor(gf_multiplication_by_9(S[2]), gf_multiplication_by_2(S[3]))
    s_11 = perform_xor(gf_multiplication_by_2(S[2]), gf_multiplication_by_9(S[3]))
    S = [s_00, s_10, s_01, s_11]
    # %%
    # Inverse Shift rows
    S[1], S[3] = S[3], S[1]
    # %%
    # Inverse Nibble substitution
    inv_nib_sub = []
    inv_nib_sub += sub_inv_niv(" ".join([S[0], S[1]])).split()
    inv_nib_sub += sub_inv_niv(" ".join([S[2], S[3]])).split()
    # %%
    # Add Round 0 Key_0
    # Perform XOR between inverse bibble substitution and k_0
    add_round_k_0 = []
    for i in range(4):
        add_round_k_0.append(perform_xor(inv_nib_sub[i], k_0.split()[i]))
    decrypted_bin = add_round_k_0
    print("Decrypted binary: ", " ".join(decrypted_bin))
    return chr(int(decrypted_bin[0] + decrypted_bin[1], 2)) + chr(int(decrypted_bin[2] + decrypted_bin[3], 2))


def manual_decryption(key, ciphertext):
    try:
        bin_cipher = bin(int(base64.b64decode(ciphertext).decode()))[2:].zfill(16)
    except Exception:
        raise ValueError("Invalid ciphertext")
    key_bin, message_bin = (lambda x: fill_zeros(bin(ord(x[0]))[2:], 8) +
                      fill_zeros(bin(ord(x[1]))[2:], 8)
            )(key), bin_cipher
    message_bin = " ".join(re.findall("....", message_bin))
    k_0, k_1, k_2 = keys_generation(key_bin)
    return decryption(message_bin, k_0, k_1, k_2)


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "-o",
        "--operation",
        required=True,
        help="Operation to execute"
    )
    ap.add_argument(
        "-k",
        "--key",
        required=False,
        help="Two character key"
    )
    ap.add_argument(
        "-m",
        "--message",
        required=False,
        help="Message encrypted or two character message to encrypt"
    )
    args = vars(ap.parse_args())
    if args["operation"] not in ["encrypt", "decrypt"]:
        raise ValueError("Operation must be either encrypt or decrypt")
    key_bin, message_bin = inputs(args["operation"], args["key"], args["message"])
    message_bin = " ".join(re.findall("....", message_bin))
    k_0, k_1, k_2 = keys_generation(key_bin)
    if args["operation"] == "encrypt":
        # -o encrypt -k Jõ -m ×(
        print("Encoded cipher text: ", encryption(message_bin, k_0, k_1, k_2))
        print(encryption(message_bin, k_0, k_1, k_2))
    else:
        # -o decrypt -k Jõ -m OTQ1Mg==
        print("Decrypted message: ", decryption(message_bin, k_0, k_1, k_2))
        print(decryption(message_bin, k_0, k_1, k_2))
